package com.tenten.computersimulator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Yoshihiko.Mori on 2016/05/24.
 */
public class ComputerTest {

    private Computer mComputer;

    @Before
    public void setUp() throws Exception {
        mComputer = new Computer(100, null);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSet_address() throws Exception {
        mComputer.set_address(50);
        assertTrue(mComputer.get_address() == 50);
    }

    @Test
    public void testInsert() throws Exception {
        mComputer.insert("PRINT");
        assertTrue(mComputer.getInsertedString().equals("PRINT"));
    }

    @Test
    public void testInsert2Params() throws Exception {
        mComputer.insert("PUSH", 6);
        assertTrue(mComputer.getInsertedString().equals("PUSH 6"));
    }

    @Test
    public void testExecute() throws Exception {
        mComputer.set_address(0);
        mComputer.insert("PUSH", 1009);
        mComputer.insert("STOP");
        mComputer.set_address(0);
        mComputer.execute();
        int programCounter = mComputer.getProgramCounter();
        assertTrue(programCounter == 1);
    }
}