package com.tenten.computersimulator;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoshihiko.Mori on 2016/05/22.
 */
public class Computer {
    private final String TAG = getClass().getName();
    private OutputListener mOutputListener;

    private int mProgramCounter;
    private int mStackPointer;
    private boolean mIsStop;

    private List<String> mMemoryMap;

    // <Test>
    private String mInsertedString;

    int get_address() {
        return mProgramCounter;
    }

    String getInsertedString() {
        return mInsertedString;
    }

    int getProgramCounter() {
        return mProgramCounter;
    }
    // </Test>

    /**
     * Constructor
     */
    public Computer(int size, OutputListener listener) {
        mMemoryMap = new ArrayList<String>();
        for(int index=0; index<size; index++) {
            mMemoryMap.add(null);
        }

        mStackPointer = size -1;

        mIsStop = false;
        mOutputListener = listener;
    }

    // Program Area
    public void set_address(int address) {
        mProgramCounter = address;
    }

    public void insert(String program) {
        mMemoryMap.set(mProgramCounter, program);
        mProgramCounter++;
        mInsertedString = program;
    }

    public void insert(String command, int param) {
        String program = command + " " + param;
        mMemoryMap.set(mProgramCounter, program);
        mProgramCounter++;
        mInsertedString = program;
    }

    public void execute() {
        boolean skipIncrementPC = false;

        String program = mMemoryMap.get(mProgramCounter);
        if(program.equals("MULT")) {
            mult();
        } else if(program.startsWith("CALL")) {
            String callString[] = program.split(" ");
            int address = Integer.parseInt(callString[1]);
            call(address);
            skipIncrementPC = true;
        } else if(program.equals("RET")) {
            ret();
            skipIncrementPC = true;
        } else if(program.equals("PRINT")) {
            print();
        } else if(program.equals("STOP")) {
            stop();
            skipIncrementPC = true; // <Y.Mori> 2016/05/24, Fixed: testExecute failed
        } else if(program.startsWith("PUSH")) {
            String pushString[] = program.split(" ");
            push(pushString[1]);
        }

        if(mProgramCounter > 0) {
            Log.d(TAG, "---");
            int len = mMemoryMap.size();
            for(int index=0; index<len; index++) {
                // <Y.Mori> 2016/05/24, comment out Log.d when runnigng testExecute as a test cas
                Log.d(TAG, index + ":" + mMemoryMap.get(index));
                // TODO: For JUnit, search "slf4j android junit log" as a solution.
                // </Y.Mori>
            }
            Log.d(TAG, "---");
        }

        if(!skipIncrementPC) {
            mProgramCounter++;
        }

        if(mIsStop) {
            return;
        } else {
            execute();
        }
    }

    // Data Area

    /**
     * Pop the 2 arguments from the stack, multiply them and push the result back to the stack
     */
    private void mult() {
        int param1 = Integer.parseInt(pop());
        int param2 = Integer.parseInt(pop());
        int ans = param1 * param2;

        Log.d(TAG, "ans = " + ans);
        push(String.valueOf(ans));
    }

    /**
     *  Set the program counter (PC) to `addr` and call a sub routine.
     * @param address
     */
    private void call(int address) {
        mProgramCounter = address;
    }


    /**
     * Pop address from stack and set PC to addresss
     */
    private void ret() {
        mProgramCounter = Integer.parseInt(pop());
    }

    /**
     * Exit the program
     */
    private void stop() {
        mIsStop = true;
    }

    /**
     * Pop value from stack and print it
     */
    private void print() {
        String value = pop();
        Log.d(TAG, "value = " + value);
        mOutputListener.onPrint(value);
    }

    /**
     * Push argument to the stack
     * @param argument
     */
    private void push(String argument) {
        int indexToRemove = -1;
        for(int index = mStackPointer; true; index--) {
            if(mMemoryMap.get(index) == null) {
                indexToRemove = index;
                break;
            }
        }

        if(indexToRemove != -1) {
            mMemoryMap.remove(indexToRemove);
        }

        mMemoryMap.add(mStackPointer, argument);
    }

    /**
     * Pop value from stack
     * @return Value on the top of the stack (data area)
     */
    private String pop() {
        String ans = mMemoryMap.get(mStackPointer);
        mMemoryMap.remove(mStackPointer);

        int indexToInsert = -1;
        for(int index = mStackPointer-1; true; index--) {
            if(mMemoryMap.get(index) == null) {
                indexToInsert = index;
                break;
            }
        }

        mMemoryMap.add(indexToInsert, null);
        return ans;
    }
}
