package com.tenten.computersimulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OutputListener {
    private TextView mResultTextView;
    private String mText = "";
    private Button mStartButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultTextView = (TextView) findViewById(R.id.resultView);

        mStartButton = (Button) findViewById(R.id.button);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main();
            }
        });

    }

    /**
     * This is a main programing in Interface section of the Programing Assignment.
     */
    private void main() {
        final int PRINT_TENTEN_BEGIN = 50;
        final int MAIN_BEGIN = 0;

        // Create new computer with a stack of 100 address
        Computer computer = new Computer(100, this);

        //Instructions for the print_tenten function
        computer.set_address(PRINT_TENTEN_BEGIN);
        computer.insert("MULT");
        computer.insert("PRINT");
        computer.insert("RET");

        //The start of the main function
        computer.set_address(MAIN_BEGIN);
        computer.insert("PUSH", 1009);
        computer.insert("PRINT");

        // Return address for when print_tenten function finishes
        computer.insert("PUSH", 6);

        // Setup arguments for calling print_tenten
        computer.insert("PUSH", 101);
        computer.insert("PUSH", 10);
        computer.insert("CALL", PRINT_TENTEN_BEGIN);

        // Stop the program
        computer.insert("STOP");

        computer.set_address(MAIN_BEGIN);
        computer.execute();
    }

    /**
     * Callback when PRINT is called.
     * @param value Value of "PRINT value".
     */
    @Override
    public void onPrint(String value) {
        if (value == null) {
            mResultTextView.setText("error");
        } else {
            mText += "# " + value + "\n";
            mResultTextView.setText(mText);
        }
    }
}
