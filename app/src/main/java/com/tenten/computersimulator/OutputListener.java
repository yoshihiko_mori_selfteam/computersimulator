package com.tenten.computersimulator;

import java.util.EventListener;

/**
 * Created Yoshihiko.Mori on 2016/05/22.
 */
public interface OutputListener extends EventListener {
    void onPrint(String value);
}
