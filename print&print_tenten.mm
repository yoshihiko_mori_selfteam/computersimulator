<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1463816902997" ID="ID_467948677" MODIFIED="1463817350582" TEXT="my mind">
<node CREATED="1463816911017" ID="ID_1160956505" MODIFIED="1463816936932" POSITION="right" TEXT="Instruction (key point to implement as logics)">
<node CREATED="1463816939186" ID="ID_1081794986" MODIFIED="1463816964369" TEXT="PUSH">
<node CREATED="1463816969153" ID="ID_603853330" MODIFIED="1463817070696" TEXT="ArrayList.add(argument)"/>
</node>
<node CREATED="1463816954034" ID="ID_876416140" MODIFIED="1463816957649" TEXT="RET">
<node CREATED="1463816989981" ID="ID_1801954392" MODIFIED="1463817021912" TEXT="ArralyList.get(&lt;last item&apos;s index&gt;)"/>
</node>
</node>
<node CREATED="1463817097283" ID="ID_222705968" MODIFIED="1463817333789" POSITION="right" TEXT="Source code (proto type for Stack 50 - 52)">
<cloud COLOR="#ccffff"/>
<node CREATED="1463817103480" ID="ID_1671110591" MODIFIED="1463817134566" TEXT="computer = Computer.new(100)"/>
<node CREATED="1463817136787" ID="ID_269904802" MODIFIED="1463817164890" TEXT="computer.set_address(PRINT_TENTEN_BEGIN)">
<node CREATED="1463817176930" ID="ID_1643200905" MODIFIED="1463817176930" TEXT=""/>
</node>
<node CREATED="1463817168512" ID="ID_822797759" MODIFIED="1463817218904" TEXT="insert(&quot;MULT&quot;)"/>
<node CREATED="1463817219684" ID="ID_8373244" MODIFIED="1463817234652" TEXT="insert(&quot;PRINT&quot;)"/>
<node CREATED="1463817235373" ID="ID_156594898" MODIFIED="1463817246587" TEXT="insert(&quot;RET&quot;)"/>
<node CREATED="1463817833871" ID="ID_999772676" MODIFIED="1463817887983" TEXT="computer.inster(&quot;STOP&quot;)"/>
<node CREATED="1463817890750" ID="ID_1936164148" MODIFIED="1463817932419" TEXT="computer.set_address(MAIN_BEGIN).excute()"/>
</node>
<node CREATED="1463817370975" ID="ID_254448940" MODIFIED="1463817373997" POSITION="right" TEXT="Rest">
<node CREATED="1463817375367" ID="ID_1354314752" MODIFIED="1463817396887" TEXT="Maybe easy once Stack 50 - 52 are OK"/>
</node>
<node CREATED="1463817407658" ID="ID_1274838566" MODIFIED="1463817414326" POSITION="left" TEXT="Look&amp;Feel">
<node CREATED="1463817490907" ID="ID_169641934" MODIFIED="1463817499692" TEXT="At first">
<node CREATED="1463817514048" ID="ID_1442160354" MODIFIED="1463817526906" TEXT="Start button"/>
<node CREATED="1463817527506" ID="ID_1844368017" MODIFIED="1463817551005" TEXT="TextView or toast to show the result"/>
</node>
<node CREATED="1463817501402" ID="ID_863250715" MODIFIED="1463817511816" TEXT="After asking">
<node CREATED="1463817557296" ID="ID_357042175" MODIFIED="1463817567151" TEXT="Better to have a popup?"/>
</node>
</node>
<node CREATED="1463817415866" ID="ID_1529434981" MODIFIED="1463817421691" POSITION="left" TEXT="Unit Test">
<node CREATED="1463817424878" ID="ID_558998453" MODIFIED="1463817471117" TEXT="Test harness for Instructions">
<node CREATED="1463817580234" ID="ID_755633692" MODIFIED="1463817591437" TEXT="Buttons">
<node CREATED="1463817592547" ID="ID_1941357926" MODIFIED="1463817595263" TEXT="MULT">
<node CREATED="1463817650109" ID="ID_1874376477" MODIFIED="1463817654864" TEXT="ommit"/>
</node>
<node CREATED="1463817595913" ID="ID_364268510" MODIFIED="1463817600828" TEXT="CALL addr"/>
<node CREATED="1463817601500" ID="ID_1930657645" MODIFIED="1463817603865" TEXT="RET"/>
<node CREATED="1463817604295" ID="ID_1387349311" MODIFIED="1463817607244" TEXT="STOP">
<node CREATED="1463817628995" ID="ID_541188729" MODIFIED="1463817631023" TEXT="ommit"/>
</node>
<node CREATED="1463817607664" ID="ID_718897951" MODIFIED="1463817610333" TEXT="PRINT">
<node CREATED="1463817633278" ID="ID_1018864023" MODIFIED="1463817635773" TEXT="ommit"/>
</node>
<node CREATED="1463817610793" ID="ID_1848269156" MODIFIED="1463817616798" TEXT="PUSH arg"/>
</node>
</node>
</node>
</node>
</map>
